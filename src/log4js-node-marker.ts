import { LoggingEvent, Layout, Logger, levels } from 'log4js';
import * as Debug from 'debug';

/**
 * @class
 *
 * Base implementation conforming to {@link https://logging.apache.org/log4j/2.x/log4j-api/apidocs/index.html} for
 * type inspection purposes.
 */
export class LogMarker {
    private name: string;
    private parents: LogMarker[];

    constructor(name: string) {
        this.name = name;
        this.parents = [];
    }

    addParents(...parents: LogMarker[]): LogMarker {
        this.parents.push(...parents);
        return this;
    }

    getName(): string {
        return this.name;
    }

    getParents(): LogMarker[] {
        return this.parents.slice();
    }

    hasParents(): boolean {
        return this.parents.length > 0;
    }

    removeParent(parent: LogMarker): boolean {
        for (let i = 0; i < this.parents.length; i++) {
            const p = this.parents[i];
            if (p.equals(parent)) {
                this.parents.splice(i, 1);
                return true;
            }
        }

        return false;
    }

    setParents(...parents: LogMarker[]): LogMarker {
        this.parents = parents.slice();
        return this;
    }

    isInstanceOf(marker: LogMarker): boolean {
        if (this.equals(marker)) {
            return true;
        } else {
            for (let parent of this.getParents()) {
                if (parent.isInstanceOf(marker)) {
                    return true;
                }
            }
        }

        return false;
    }

    isInstanceOfByName(name: string): boolean {
        if (this.name === name) {
            return true;
        } else {
            for (let parent of this.getParents()) {
                if (parent.isInstanceOfByName(name)) {
                    return true;
                }
            }
        }

        return false;
    }

    equals(marker: LogMarker): boolean {
        return this.name === marker.getName();
    }
}

/**
 * Rather than new-up instances of {LogMarker}s all the time, it can be more
 * convenient to use a container to track which markers have been instantiated already.
 * This is especially true if someone has overridden how {LogMarker} equality or instantiation is performed.
 * Also provided for API similarity with log4j.
 */
export interface IMarkerContainer {

    /**
     * Has a {LogMarker} of a specified name already been registered with this container?
     * @param {string} markerName
     * @returns {boolean}
     */
    exists(markerName: string): boolean;

    /**
     * Retrieve a {LogMarker} of a specified name from the container,
     * or instantiate one with that name if it doesn't exist.
     * @param {string} markerName
     * @returns {LogMarker}
     */
    getMarker(markerName: string): LogMarker;

    /**
     * Remove all {LogMarker}s registered with the container.
     */
    clear(): void;
}

/*
 * The log4js library makes it very easy to add layouts, but extraordinarily difficult to extend loggers or the
 * logging event itself.
 *
 * Consequently, we have to monkey patch in some of these things.
 */

// tslint:disable-next-line
import log4jsModule = require('log4js');
// tslint:disable-next-line
const log4jsLogger: any = require('log4js/lib/logger');

const debug = Debug('log4js-marker:logger');

/*
 * First, we must extend the log4js module itself to add our marker-accepting logging methods to the Logger interface.
 * And to add a markers property to the LoggingEvent.
 */
declare module 'log4js' {

    /**
     * Because of the variadic arguments for the {Logger}s logging methods, we go the
     * safe route and define new methods for logging with markers.
     *
     * If you wrap log4js with a custom interface in your project, it should be easy
     * enough to modify that wrapper to call these methods instead of the default
     * log4js logger methods.
     */
        // tslint:disable-next-line
    interface Logger {
        traceWithMarkers(markers: LogMarker[], message: string, ...args: any[]): void;

        debugWithMarkers(markers: LogMarker[], message: string, ...args: any[]): void;

        infoWithMarkers(markers: LogMarker[], message: string, ...args: any[]): void;

        warnWithMarkers(markers: LogMarker[], message: string, ...args: any[]): void;

        errorWithMarkers(markers: LogMarker[], message: string, ...args: any[]): void;

        fatalWithMarkers(markers: LogMarker[], message: string, ...args: any[]): void;

        /*
         * We'll use this one internally for the custom LOGLEVELWithMarkers methods.
         * The log4js typings expose the normal _log method so we'll follow their lead.
         */
        _logWithMarkers(level: string, data: any, markers: LogMarker[]): void;
    }

    /**
     * Extend the {LoggingEvent} interface with a markers property.
     */
        // tslint:disable-next-line
    interface LoggingEvent {
        markers?: LogMarker[];
    }
}

/*
 * The LoggingEvent inside the implementation of log4js is an actual class, not just anonynmous objects, and we need
 * to be able to construct them. Unfortunately, the constructor is encapsulated inside the loggers module, which is
 * not exposed directly through any typings. We must do some hackery to access the constructor.
 *
 * The loggers module itself exports a function, so we just need to call it with some bogus arguments since we only
 * care about the LoggingEvent constructor.
 */
const LoggingEventCtor = log4jsLogger({ levels: [] }).LoggingEvent;

/*
 * Because the Logger class is not accessible to us for modification, we have to decorate the getLogger method to
 * attach the methods we want to add.
 */
const originalLog4JsModuleGetLogger = log4jsModule.getLogger;
log4jsModule.getLogger = (category?: string): Logger => {
    const originalLogger: Logger = originalLog4JsModuleGetLogger(category);

    originalLogger._logWithMarkers = function (this: any, level: string, data: any, markers: LogMarker[]): void {
        debug(`sending log data (${level}) to appenders`);

        const loggingEvent = <LoggingEvent> new LoggingEventCtor(this.category, level, data, this.context);
        loggingEvent.markers = markers;

        this.dispatch(loggingEvent);
    }.bind(originalLogger);

    /*
     * log4js allows us to define custom levels and it auto-generates logger methods for all custom levels,
     * so we should follow that pattern.
     */
    for (let level of Object.keys(levels)) {
        const levelStrLower = level.toString().toLowerCase();
        const levelMethod = levelStrLower.replace(/_([a-z])/g, g => g[1].toUpperCase());

        originalLogger[`${levelMethod}WithMarkers`] = function (this: any, markers: LogMarker[], ...args: any[]): void {
            if (this.isLevelEnabled(level)) {
                this._logWithMarkers(level, args, markers);
            }
        }.bind(originalLogger);
    }

    return originalLogger;
};

/**
 * A default singleton implementation of a {IMarkerContainer} that returns normal
 * {LogMarker} objects.
 */
export const SingletonMarkerContainer: IMarkerContainer = (() => {
    let markers: { [name: string]: LogMarker } = {};
    return {
        exists: (markerName: string): boolean => {
            return !!markers[markerName];
        },
        getMarker: (markerName: string): LogMarker => {
            markers[markerName] = markers[markerName] || new LogMarker(markerName);
            return markers[markerName];
        },
        clear: (): void => {
            markers = {};
        }
    };
})();