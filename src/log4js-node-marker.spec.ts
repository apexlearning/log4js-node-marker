import { Level } from 'log4js';
import { LogMarker, SingletonMarkerContainer } from './log4js-node-marker';

describe('log4js-marker', () => {
    describe('LogMarker', () => {
        it('should be constructed with provided name', () => {
            const expectedName = 'name';
            const m = new LogMarker(expectedName);
            expect(m.getName()).toEqual(expectedName);
        });

        it('should be constructed with no parents', () => {
            const m = new LogMarker('m');
            expect(m.hasParents()).toEqual(false);
        });

        it('should compare to other LogMarkers by name', () => {
            const expectedName = 'name';
            const m = new LogMarker(expectedName);
            const m2 = new LogMarker(expectedName);
            const m3 = new LogMarker('');
            expect(m.equals(m2)).toEqual(true);
            expect(m2.equals(m)).toEqual(true);
            expect(m.equals(m3)).toEqual(false);
        });

        it('should add parents', () => {
            const p1 = new LogMarker('parent1');
            const p2 = new LogMarker('parent2');
            const c = new LogMarker('child');
            c.addParents(p1, p2);

            expect(c.hasParents()).toEqual(true);
            expect(c.getParents()).toEqual([p1, p2]);
        });

        it('should set parents', () => {
            const p1 = new LogMarker('parent1');
            const p2 = new LogMarker('parent2');
            const c = new LogMarker('child');
            c.addParents(p1, p2);
            c.setParents(p1);

            expect(c.hasParents()).toEqual(true);
            expect(c.getParents()).toEqual([p1]);
        });

        it('should remove parent and report removal', () => {
            const p1 = new LogMarker('parent1');
            const c = new LogMarker('child');
            c.addParents(p1);
            const removed = c.removeParent(p1);

            expect(removed).toEqual(true);
            expect(c.hasParents()).toEqual(false);
        });

        it('should not remove parent and report lack of removal if object isnt a parent', () => {
            const p1 = new LogMarker('parent1');
            const c = new LogMarker('child');
            c.addParents(p1);
            const removed = c.removeParent(new LogMarker('parent2'));

            expect(removed).toEqual(false);
            expect(c.getParents()).toEqual([p1]);
        });

        it('should be considered instance of self', () => {
            const expectedName = 'name';
            const m = new LogMarker(expectedName);
            const m2 = new LogMarker(expectedName);

            expect(m.isInstanceOf(m2)).toEqual(true);
            expect(m.isInstanceOfByName(m2.getName())).toEqual(true);
            expect(m2.isInstanceOf(m)).toEqual(true);
            expect(m2.isInstanceOfByName(m.getName())).toEqual(true);
        });

        it('should be considered instance of immedate parent', () => {
            const p = new LogMarker('parent');
            const c = new LogMarker('child');
            c.addParents(p);

            expect(c.isInstanceOf(p)).toEqual(true);
            expect(c.isInstanceOfByName(p.getName())).toEqual(true);
            expect(p.isInstanceOf(c)).toEqual(false);
            expect(p.isInstanceOfByName(c.getName())).toEqual(false);
        });

        it('should be considered instance of ancestors', () => {
            const gp = new LogMarker('grand-parent');
            const p = new LogMarker('parent');
            const c = new LogMarker('child');
            p.addParents(gp);
            c.addParents(p);

            expect(c.isInstanceOf(gp)).toEqual(true);
            expect(c.isInstanceOfByName(gp.getName())).toEqual(true);
            expect(gp.isInstanceOf(c)).toEqual(false);
            expect(gp.isInstanceOfByName(c.getName())).toEqual(false);
        });

    });

    describe('SingletonMarkerContainer', () => {
        beforeEach(() => {
            SingletonMarkerContainer.clear();
        });

        it('should create new marker with no parents if the requested one does not exist', () => {
            const markerName = 'markerName';

            expect(SingletonMarkerContainer.exists(markerName)).toEqual(false);
            const m = SingletonMarkerContainer.getMarker(markerName);

            expect(m.isInstanceOfByName(markerName)).toEqual(true);
            expect(m.hasParents()).toEqual(false);
            expect(SingletonMarkerContainer.exists(markerName)).toEqual(true);
        });

        it('should return an existing marker if one exists', () => {
            const markerName = 'markerName';
            const parentMarker = new LogMarker('parent');

            expect(SingletonMarkerContainer.exists(markerName)).toEqual(false);
            SingletonMarkerContainer.getMarker(markerName).addParents(parentMarker);
            const m = SingletonMarkerContainer.getMarker(markerName);

            expect(m.isInstanceOfByName(markerName)).toEqual(true);
            expect(m.getParents()).toEqual([parentMarker]);
            expect(m.isInstanceOf(parentMarker)).toEqual(true);
        });
    });
});

class MockLevel implements Level {
    isEqualTo(other: string): boolean;
    isEqualTo(otherLevel: Level): boolean;
    isEqualTo(other: string | Level): boolean {
        return false;
    }

    isGreaterThanOrEqualTo(other: string): boolean;
    isGreaterThanOrEqualTo(otherLevel: Level): boolean;
    isGreaterThanOrEqualTo(other: string | Level): boolean {
        return false;
    }

    isLessThanOrEqualTo(other: string): boolean;
    isLessThanOrEqualTo(otherLevel: Level): boolean;
    isLessThanOrEqualTo(other: string | Level): boolean {
        return false;
    }

}