import { Level, LoggingEvent, PatternLayout } from 'log4js';
import { SingletonMarkerContainer } from './log4js-node-marker';
import { MarkerSelector } from './selector';

describe('log4js-marker/selector', () => {
    describe('MarkerSelector', () => {
        beforeEach(() => {
            SingletonMarkerContainer.clear();
        });

        it('should match log events whose data includes at least one marker that is an instance of the configured name', () => {
            const matchingMarker = SingletonMarkerContainer.getMarker('matching');

            const matchingChildMarker = SingletonMarkerContainer.getMarker('matchingChild');
            matchingChildMarker.addParents(matchingMarker);

            const nonMatchMarker = SingletonMarkerContainer.getMarker('not-matching');

            const layout: PatternLayout = {
                type: 'pattern',
                pattern: 'p'
            };

            const logEvent: LoggingEvent = {
                categoryName: 'default',
                level: new MockLevel(),
                markers: [matchingMarker, nonMatchMarker],
                data: ['message'],
                startTime: new Date(),
                pid: 1,
                context: {}
            };

            const logEventOfChild: LoggingEvent = {
                categoryName: 'default',
                level: new MockLevel(),
                markers: [matchingChildMarker, nonMatchMarker],
                data: ['message'],
                startTime: new Date(),
                pid: 1,
                context: {}
            };

            const mps = new MarkerSelector(SingletonMarkerContainer, matchingMarker.getName(), layout);
            expect(mps.match(logEvent)).toEqual(true);
            expect(mps.match(logEventOfChild)).toEqual(true);
        });

        it('should not match log events whose data does not include at least one marker matching configured name', () => {
            const expectedMarker = SingletonMarkerContainer.getMarker('matching');
            const nonMatchMarker = SingletonMarkerContainer.getMarker('not-matching');
            const layout: PatternLayout = {
                type: 'pattern',
                pattern: 'p'
            };

            const logEvent: LoggingEvent = {
                categoryName: 'default',
                level: new MockLevel(),
                markers: [nonMatchMarker],
                data: ['message'],
                startTime: new Date(),
                pid: 1,
                context: {}
            };

            const mps = new MarkerSelector(SingletonMarkerContainer, expectedMarker.getName(), layout);
            expect(mps.match(logEvent)).toEqual(false);
        });

    });
});

class MockLevel implements Level {
    isEqualTo(other: string): boolean;
    isEqualTo(otherLevel: Level): boolean;
    isEqualTo(other: string | Level): boolean {
        return false;
    }

    isGreaterThanOrEqualTo(other: string): boolean;
    isGreaterThanOrEqualTo(otherLevel: Level): boolean;
    isGreaterThanOrEqualTo(other: string | Level): boolean {
        return false;
    }

    isLessThanOrEqualTo(other: string): boolean;
    isLessThanOrEqualTo(otherLevel: Level): boolean;
    isLessThanOrEqualTo(other: string | Level): boolean {
        return false;
    }

}