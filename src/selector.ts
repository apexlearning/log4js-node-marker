import {
    ISelector,
    SelectorFactoryFunc,
    ISelectorConfiguration
} from '@apexlearning/log4js-node-selectorlayout-selectortypings';
import { IMarkerContainer, SingletonMarkerContainer } from './log4js-node-marker';
import { Layout, LoggingEvent } from 'log4js';

/**
 * A log4js-node-selectorlayout configuration for selecting based on the presence of a {LogMarker}.
 */
export interface IMarkerSelectorConfiguration extends ISelectorConfiguration {
    type: 'marker';
    matchMarkerName: string;
}

/**
 * A log4js-node-selectorlayout {ISelector} which matches {LoggingEvent}s that
 * contain a specifically named {LogMarker}.
 */
export class MarkerSelector implements ISelector {
    private readonly markerContainer: IMarkerContainer;
    private readonly matchMarkerName: string;
    private readonly layout: Layout;

    constructor(markerContainer: IMarkerContainer, matchMarkerName: string, layout: Layout) {
        this.markerContainer = markerContainer;
        this.matchMarkerName = matchMarkerName;
        this.layout = layout;
    }

    match(loggingEvent: LoggingEvent): boolean {
        const matchMarker = this.markerContainer.getMarker(this.matchMarkerName);

        if (loggingEvent.markers) {
            return loggingEvent.markers.some((m) => m.isInstanceOf(matchMarker));
        }

        return false;
    }

    getLayout(): Layout {
        return this.layout;
    }
}

/**
 * A convenience {SelectorFactoryFunc} to create a {MarkerSelector}.
 * @param {IMarkerContainer} markerContainer
 * @returns {SelectorFactoryFunc}
 */
export const markerSelectorFactory = (markerContainer?: IMarkerContainer): SelectorFactoryFunc => {
    const container = markerContainer || SingletonMarkerContainer;

    return (config: ISelectorConfiguration): MarkerSelector => {
        const castConfig = <IMarkerSelectorConfiguration> config;
        return new MarkerSelector(container, castConfig.matchMarkerName, castConfig.layout);
    };
};