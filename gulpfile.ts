import * as gulp from 'gulp';

const clean = require('gulp-clean');
const jasmine = require('gulp-jasmine');
const runSequence = require('run-sequence');
const sourcemaps = require('gulp-sourcemaps');
const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
const fs = require('fs');
const path = require('path');
const EOL = require('os').EOL;
const ts = require('gulp-typescript');
const tslint = require('gulp-tslint');
const tsProject = ts.createProject('tsconfig.json');

const DIST_NPMRC_PATH = path.resolve('dist', '.npmrc');

gulp.task('build', (done) => {
    runSequence('clean', 'transpile', done);
});

gulp.task('dist', ['lint', 'test', 'build'], (done) => {
    runSequence('copy', 'npmrc', done);
});

gulp.task('npmrc', () => {
    const npmrcContents = [
        'registry=https://registry.npmjs.org/',
        '//registry.npmjs.org/:_authToken=${NPM_TOKEN}',
        ''
    ].join(EOL);

    fs.writeFileSync(DIST_NPMRC_PATH, npmrcContents);
});

gulp.task('clean', () => {
    return gulp.src(['dist', 'transpile'])
        .pipe(clean());
});

gulp.task('lint', () => {
    return tsProject.src()
        .pipe(tslint({
            formatter: 'verbose'
        }))
        .pipe(tslint.report({
            emitError: true
        }));
});

gulp.task('transpile', () => {
    return tsProject.src()
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('transpile'));
});

gulp.task('copy', () => {
    return gulp.src(
        [
            'transpile/**/*.js',
            'transpile/**/*.d.ts',
            '!transpile/**/*.spec.js',
            '!transpile/**/*.spec.d.ts',
            '!transpile/spec_helpers/*',
            'README.md',
            'package.json',
            'LICENSE'
        ])
        .pipe(gulp.dest('dist'));
});

gulp.task('test', ['build'], () => {
    return gulp.src('./transpile/**/*.spec.js')
        .pipe(jasmine({
            config: {
                random: false,
                stopSpecOnExpectationFailure: false,
                helpers: [
                    './transpile/spec_helpers/*.js'
                ]
            },
            reporter: [
                new SpecReporter({
                    colors: {
                        enabled: process.argv.indexOf('--no-color') === -1
                    },
                    spec: {
                        displayPending: true,
                        displayStacktrace: true
                    }
                })
            ]
        }));
});