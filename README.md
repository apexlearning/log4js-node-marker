# log4js-node-marker

An extension to [log4js-node][1] to associate special markers with log messages.
It's basically the log4js-node equivalent of [log4j Markers][3].

Associating markers with log messages are a convenient way of being able to perform
log message filtering, such as by using the MarkerSelector in combination with
[log4js-node-selectorlayout][2].

## Usage

Install the package as a dependency of your project

    npm install --save log4js-node-marker

Now you can log messages with markers as follows

    const marker = SingletonMarkerContainer.getMarker('MyMarker');
    const logger = log4js.getLogger();
    logger.infoWithMarkers([marker], 'log message');

If you don't the look of the `LEVELWithMarkers` methods, a possible resolution is
to write a wrapper type around log4js-node's `Logger` which calls the `LEVELWithMarkers`
methods behind the scenes. We chose not to override the signature of the normal
log4js-node `info`, `debug`, etc. methods to avoid breaking code that is already using
log4js-node but wants to start using markers.

### MarkerSelector

This package also includes a selector compatible with [log4js-node-selectorlayout][2] called the MarkerSelector.
To use the selector, first add it to your log4js-node configuration. For example:

    {
      "options": {
        "categories": {
          "default": {
            "appenders": [ "console" ],
            "level": "info"
          }
        },
        "appenders": {
          "console": {
            "type": "console",
            "layout": {
              "type": "selector",
              "defaultLayout": {
                "type": "pattern",
                "pattern": "%[[%d] [%p] [%c]%] - %m"
              },
              "selectors": [
                {
                  "type": "marker",
                  "matchMarkerName": "JSON",
                  "layout": {
                    "type": "pattern",
                    "pattern": "%m"
                  }
                }
              ]
            }
          }
        }
      }
    }

Next, register the marker selector when you register the selector layout in your application.
For example:

    const selectorRegistrations = {
        'marker': markerSelectorFactory()
    };
    log4js.addLayout('selector', selectorLayoutRegistrar(selectorRegistrations));
    log4js.configure(log4jsConfigObject);

## License Information

Copyright 2018 Apex Learning Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: https://github.com/log4js-node/log4js-node/
[2]: https://bitbucket.org/apexlearning/log4js-node-selectorlayout/
[3]: https://logging.apache.org/log4j/2.0/manual/markers.html